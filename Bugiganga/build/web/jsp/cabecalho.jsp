<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bugiganga</title>
        <link rel="stylesheet" type="text/css" href="css/base.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="js/ajaxjs.js"></script>        
    </head>
    <body>
        <div id="tudo">     
            <center>
                <br>&nbsp;
                <img src="imagens/logo.png"/><br>
                <h1>
                    Sua Loja Virtual
                    <!--<img width="50" src="../imagem/livro.gif">-->
                </h1>
                <c:choose>
                    <c:when test="${ user eq null }">
                        <jsp:forward page="index.jsp" />
                    </c:when>
                    <c:otherwise>
                        <p>Bem-vindo, ${ user.nome }</p>
                    </c:otherwise>
                </c:choose>
                <form action="logout" method="post">
                    <input type="submit" value="Sair"/>
                </form>
                <%
                    // caso exista uma mensagem na requisição, exibir
                    if (request.getAttribute("mensagem") != null) {
                %>
                <div style="color: red;"><%= request.getAttribute("mensagem")%></div>
                <%
                    }
                %>
            </center>
            <hr>