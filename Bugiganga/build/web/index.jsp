<%-- 
    Document   : index
    Created on : 23/11/2013, 13:24:40
    Author     : Joelma
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bugiganga</title>
        <link rel="stylesheet" type="text/css" href="css/base.css">
    </head>
    <body>
        <div id="tudo1">
            <img src="imagens/logo.png"/>
            <h1>Bugiganga</h1>
            <form action="Login" method="post">
                <div>Login:   <input type="text" name="login" /></div>
                <div>Senha: <input type="password" name="password" /></div>
                <div><input type="submit" value="Entrar" /></div>
            </form>
            <a href="cadastroCliente.jsp" id="cadastrar">Cadastrar</a>
            <%
                // caso exista uma mensagem na requisição, exibir
                if (request.getAttribute("mensagem") != null) {
            %>
            <div style="color: red;"><%= request.getAttribute("mensagem")%></div>
            <%
                }
            %>

            <%@ include file="jsp/rodape.jsp" %>
        </div>
    </body>
</html>
