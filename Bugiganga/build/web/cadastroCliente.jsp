<%-- 
    Document   : cadastroCliente
    Created on : 28/11/2013, 09:24:23
    Author     : Joelma
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bugiganga</title>
        <link rel="stylesheet" type="text/css" href="css/base.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="js/ajaxjs.js"></script>
        <script type="text/javascript" src="js/jquery.pstrength-min.1.2.js"></script>

    </head>
    <body>

        <div id="tudo_cad">

            <center><img src="imagens/logo.png" style="margin: 30px;"/></center>

            <h1>Bugiganga</h1><br/>

            <h2 style="text-align: center;">Cadastro</h2>

            <form id="formulario" name="formulario" accept-charset="ISO-8859-1" action="Cadastro" method="post">

                <p style="display: inline; margin-left: -79px;">* Campos obrigatórios</p><br><br>

                <div>Código* :<input type="text" name="codigo" id="codigo" class="numero" placeholder="Somente números..."/><center><p id="codigo_preenchido">O campo código precisa ser preenchido</p></center></div>
                &nbsp;&nbsp;

                <div>Nome* :&nbsp;&nbsp;<input type="text" name="nome" id="nome"/><center><p id="nome_preenchido">O campo nome precisa ser preenchido</p><p id="nome_tamanho">Preencha o campo com pelo menos 3 carateres</p></center></div>
                &nbsp;&nbsp;

                <div>Cidade :&nbsp;<input type="text" name="cidade" /></div>
                &nbsp;&nbsp;

                <div>Endereço :<input type="text" name="endereco" /></div>
                &nbsp;&nbsp;

                <div>Telefone* :<input type="text" name="telefone" id="telefone" onKeyPress="MascaraTelefone(formulario.telefone);" maxlength="14" onBlur="ValidaTelefone(formulario.telefone);"/><center><p id="telefone_preenchido">O campo telefone precisa ser preenchido</p></center></div>
                &nbsp;&nbsp;

                <div>E-mail* :<input type="email" name="email" id="email" /><center><p id="email_preenchido">O campo e-mail precisa ser preenchido</p></center></div>
                &nbsp;&nbsp;

                <div>CPF* :<input type="text" name="cpf" id="cpf" onBlur="ValidarCPF(formulario.cpf);" onKeyPress="MascaraCPF(formulario.cpf);" maxlength="14"/><center><p id="cpf_preenchido">O campo cpf precisa ser preenchido</p></center></div>
                &nbsp;&nbsp;

                <div>Login* :<input type="text" name="login" id="login" /><center><p id="login_preenchido">O campo login precisa ser preenchido</p></center></div>
                &nbsp;&nbsp;

                <div style="width: 216px; margin-left: 841px;">Senha* :<input type="password" name="password" id="password" /><center><p id="senha_preenchida">O campo senha precisa ser preenchido</p></center></div>
                &nbsp;&nbsp;

                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.jsp">Cancelar</a>&nbsp;&nbsp;<input type="submit" value="Cadastrar" id="Cadastrar" /></div>

            </form>

            <br/><br/><br/><br/>

            <%@include file="jsp/rodape.jsp" %>

        </div>
    </body>
</html>
