<%-- 
    Document   : comprar
    Created on : 05/12/2013, 22:22:41
    Author     : Joelma
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div id="tudo1">
    <%@ include file="../jsp/header.jsp" %>
    <p>Valor total da compra:
        <strong>
            <fmt:formatNumber value="${sessionScope.cart.total}" type="currency"/>
        </strong>
    <p>Para efetuar a compra dos produtos selecionados, 
        informe os seguintes dados:
        <c:url var="url" value="/produtos/recibo" />
    <form action="${url}" method="post">
        <table summary="layout">
            <tr>
                <td><strong>Nome do Cliente:</strong></td>
                <td><div id="user">${ user.nome }</div></td>
            </tr>
            <tr>
                <td><strong>Número do cartão:</strong></td>
                <td><input type="text" name="cardnum" 
                           value="xxxx xxxx xxxx xxxx" size="19"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Comprar"></td>
            </tr>
        </table>
    </form>
    <%@ include file="../jsp/rodape.jsp" %>
</div>
