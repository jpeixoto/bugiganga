<%-- 
    Document   : recibo
    Created on : 05/12/2013, 22:26:08
    Author     : Joelma
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div id="tudo1">
    <%@ include file="../jsp/header.jsp" %>
    <h3>Obrigado, ${ user.nome }.</h3><br> 
    <jsp:useBean id="now" class="java.util.Date" />
    <jsp:setProperty name="now" property="time" value="${now.time + 432000000}" />
    Seus produtos serão enviados para você em
    <fmt:formatDate value="${now}" type="date" dateStyle="full"/>.<br><br>
    <c:remove var="cart" scope="session" />
    <c:url var="url" value="/produtos/catalogo.jsp" />
    <!--<strong>
        <a href="${url}">Continuar comprando</a>
        &nbsp;&nbsp;&nbsp;</strong>-->
    <%@ include file="../jsp/rodape.jsp" %>
</div>

