/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bugiganga.negocio;

/**
 *
 * @author Joelma
 */
import java.util.List;
import bugiganga.negocio.excecoes.CompraException;
import bugiganga.negocio.excecoes.ProdutoNaoEncontradoException;
import java.sql.SQLException;

public class BugigangaBean {

    private Bugiganga sistema = null;
    private String idProduto = "0";

    public BugigangaBean() {
    }

    public void setIdProduto(String id) {
        this.idProduto = id;
    }

    public void setSistema(Bugiganga bugiganga) {
        this.sistema = bugiganga;
    }

    public Produto getProduto() throws ProdutoNaoEncontradoException, SQLException {
        return (Produto) sistema.getProduto(idProduto);
    }

    public List<Produto> getProdutos() throws Exception {
        return sistema.getProdutos();
    }

    public void comprarProdutos(CarrinhoCompras cart) throws CompraException, SQLException {
        sistema.comprarProdutos(cart);
    }

}
