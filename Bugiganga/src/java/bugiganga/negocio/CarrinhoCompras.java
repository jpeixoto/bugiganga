/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bugiganga.negocio;

import java.util.*;

public class CarrinhoCompras {

    Map<String, ItemCompra> itens;

    public CarrinhoCompras() {
        itens = new HashMap<String, ItemCompra>();
    }

    public synchronized void adicionar(Produto produto) {
        if (itens.containsKey(produto.getIdProduto())) {
            ItemCompra item = itens.get(produto.getIdProduto());
            item.incrementaQuantidade();
        } else {
            ItemCompra novoItem = new ItemCompra(produto);
            itens.put(produto.getIdProduto(), novoItem);
        }
    }

    public synchronized void remover(String idProduto) {
        if (itens.containsKey(idProduto)) {
            ItemCompra item = itens.get(idProduto);
            item.decrementaQuantidade();
            if (item.getQuantidade() <= 0) {
                itens.remove(idProduto);
            }

        }
    }

    public synchronized List<ItemCompra> getItens() {
        List<ItemCompra> resultado = new ArrayList<ItemCompra>();
        resultado.addAll(this.itens.values());
        return resultado;
    }

    protected void finalize() throws Throwable {
        itens.clear();
    }

    public synchronized int getNumeroItens() {
        int numeroItens = 0;
        for (ItemCompra item : getItens()) {
            numeroItens += item.getQuantidade();
        }
        return numeroItens;
    }

    public synchronized double getTotal() {
        double total = 0.0;
        for (ItemCompra item : getItens()) {
            Produto produto = item.getItem();
            total = total + (item.getQuantidade() * produto.getPreco());
        }
        return total;
    }

    public void limpar() {
        itens.clear();
    }
}
