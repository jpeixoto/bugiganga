/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bugiganga.negocio;

/**
 *
 * @author Joelma
 */
public class ItemCompra {

    private Produto item;
    private int quantidade;

    public ItemCompra(Produto prod) {
        item = prod;
        quantidade = 1;
    }

    public void incrementaQuantidade() {
        quantidade++;
    }

    public void decrementaQuantidade() {
        quantidade--;
    }

    public Produto getItem() {
        return item;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantity) {
        this.quantidade = quantity;
    }

}
