/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bugiganga.negocio;

/**
 *
 * @author Joelma
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import bugiganga.negocio.excecoes.CompraException;
import bugiganga.negocio.excecoes.ProdutoNaoEncontradoException;
import java.sql.SQLException;
import loja.bd.RepositorioProdutosJDBC;

public class Bugiganga {

    private RepositorioProdutosJDBC estoqueProdutos;

//    private List<Produto> estoqueProdutos;
    public Bugiganga() {

        //estoqueProdutos = new ArrayList<Produto>();
        estoqueProdutos = new RepositorioProdutosJDBC();
        popularProdutos();
    }

    private void popularProdutos() {

    }

    public List<Produto> getProdutos() throws Exception {
        List<Produto> produtos;

        produtos = estoqueProdutos.getProdutos();

        return produtos;
    }

    public Produto getProduto(String idProduto) throws ProdutoNaoEncontradoException, SQLException {
        Produto produto;

        produto = estoqueProdutos.getProduto(idProduto);

        return produto;

    }

    public void comprarProdutos(CarrinhoCompras carrinho) throws CompraException, SQLException {
        Collection<ItemCompra> items = carrinho.getItens();
        Iterator<ItemCompra> i = items.iterator();

        while (i.hasNext()) {
            ItemCompra item = (ItemCompra) i.next();
            Produto produto = (Produto) item.getItem();
            String id = produto.getIdProduto();
            int quantity = item.getQuantidade();
            comprarProduto(id, quantity);
        }
    }

    public void comprarProduto(String idProduto, int qtdComprada)
            throws CompraException, SQLException {
        Produto produtoSelecionado;
        try {
            produtoSelecionado = getProduto(idProduto);
            int qtdEstoque = produtoSelecionado.getQuantidade();

            if ((qtdEstoque - qtdComprada) >= 0) {
                int novaQtd = qtdEstoque - qtdComprada;
                produtoSelecionado.setQuantidade(novaQtd);
            } else {
                throw new CompraException("Produto " + idProduto
                        + " sem estoque suficiente.");
            }
        } catch (ProdutoNaoEncontradoException e) {
            throw new CompraException(e.getMessage());
        }

//        int qtdEstoque = produtoSelecionado.getQuantidade();
//        if ((qtdEstoque - qtdComprada) >= 0) {
//            int novaQtd = qtdEstoque - qtdComprada;
//            produtoSelecionado.setQuantidade(novaQtd);
//        } else {
//            throw new CompraException("Produto " + idProduto
//            + " sem estoque suficiente.");
//        }
    }

    public void fechar() {
        // libera conexões de banco de dados
    }

//    List<Produto> getProdutos() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
}
