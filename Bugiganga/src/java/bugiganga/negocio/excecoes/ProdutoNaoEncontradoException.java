/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bugiganga.negocio.excecoes;

/**
 *
 * @author Joelma
 */
public class ProdutoNaoEncontradoException extends Exception {

    public ProdutoNaoEncontradoException() {
    }

    public ProdutoNaoEncontradoException(String msg) {
        super(msg);
    }
}
