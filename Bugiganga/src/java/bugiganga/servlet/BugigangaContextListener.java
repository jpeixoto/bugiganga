/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bugiganga.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import bugiganga.negocio.Bugiganga;

/**
 *
 * @author Joelma
 */
public final class BugigangaContextListener implements ServletContextListener {

    public static final String SISTEMA_BUGIGANGA = "sistemaBugiganga";

    public void contextInitialized(ServletContextEvent event) {
        ServletContext context = event.getServletContext();

        try {
            Bugiganga bugiganga = new Bugiganga();
            context.setAttribute(SISTEMA_BUGIGANGA, bugiganga);
        } catch (Exception ex) {
            System.out.println(
                    "O sistema de loja virtual não pode ser publicado no contexto: "
                    + ex.getMessage());
        }
    }

    public void contextDestroyed(ServletContextEvent event) {
        ServletContext context = event.getServletContext();

        Bugiganga bugiganga = (Bugiganga) context.getAttribute(SISTEMA_BUGIGANGA);

        if (bugiganga != null) {
            bugiganga.fechar();
        }

        context.removeAttribute(SISTEMA_BUGIGANGA);
    }

}
