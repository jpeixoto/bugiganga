/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bugiganga.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import bugiganga.negocio.CarrinhoCompras;
import bugiganga.negocio.Bugiganga;
import bugiganga.negocio.Produto;
import bugiganga.negocio.excecoes.CompraException;
import bugiganga.negocio.excecoes.ProdutoNaoEncontradoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

/**
 *
 * @author Joelma
 */
public class ServletControladorBugiganga extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        String idProduto = null;
        String limpar = null;
        Produto produto = null;
        Bugiganga bugiganga = (Bugiganga) getServletContext().getAttribute(
                BugigangaContextListener.SISTEMA_BUGIGANGA);
        HttpSession session = request.getSession();

        CarrinhoCompras carrinho = (CarrinhoCompras) session.getAttribute("cart");
        if (carrinho == null) {
            carrinho = new CarrinhoCompras();
            session.setAttribute("cart", carrinho);
        }

        String acaoSelecionada = request.getServletPath();
        switch (acaoSelecionada) {
            case "/produtos/catalogo":
                idProduto = request.getParameter("Add");
                if (!idProduto.equals("")) {
                    try {
                        produto = bugiganga.getProduto(idProduto);
                        carrinho.adicionar(produto);
                    } catch (ProdutoNaoEncontradoException ex) {
                        // isso não deve acontecer
                    } catch (SQLException ex) {
                        Logger.getLogger(ServletControladorBugiganga.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                break;
            case "/produtos/mostrarCarrinho":
                idProduto = request.getParameter("remover");
                if (idProduto != null) {
                    carrinho.remover(idProduto);
                }
                limpar = request.getParameter("limpar");
                if ((limpar != null) && limpar.equals("limpar")) {
                    carrinho.limpar();
                }
                break;
            case "/produtos/recibo":
                try {
                    bugiganga.comprarProdutos(carrinho);
                } catch (CompraException ex) {
                } catch (SQLException ex) {
                    Logger.getLogger(ServletControladorBugiganga.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }

        String tela = acaoSelecionada + ".jsp";

        try {
            request.getRequestDispatcher(tela).forward(request, response);
        } catch (ServletException | IOException ex) {
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        doGet(request, response);
    }

}
