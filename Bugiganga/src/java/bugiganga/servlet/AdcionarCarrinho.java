/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bugiganga.servlet;

import bugiganga.negocio.Bugiganga;
import bugiganga.negocio.ItemCompra;
import bugiganga.negocio.Produto;
import bugiganga.negocio.excecoes.ProdutoNaoEncontradoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import servicos.LoginServlet;

/**
 *
 * @author Joelma
 */
public class AdcionarCarrinho extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession();

        String produtoId = request.getParameter("Add");

        String removeProdutoId = request.getParameter("remover");

        Bugiganga bg = new Bugiganga();
        try {

            Bugiganga loja = new Bugiganga();
            try {
                List<Produto> p = loja.getProdutos();
                request.setAttribute("produtoLista", p);
            } catch (Exception ex) {
            }

            Produto p = bg.getProduto(produtoId);

            Produto dellProduto = bg.getProduto(removeProdutoId);

            ItemCompra ic = new ItemCompra(p);
            ic.setQuantidade(1);

            ItemCompra dell = new ItemCompra(dellProduto);
            dell.decrementaQuantidade();

            List<ItemCompra> icList = (List<ItemCompra>) session.getAttribute("carrinhoList");

            boolean existe = false;

            for (int i = 0; i < icList.size(); i++) {
                ItemCompra icc = icList.get(i);
                if (icc.getItem().getIdProduto().equals(ic.getItem().getIdProduto())) {
                    icc.setQuantidade(icc.getQuantidade() + 1);
                    existe = true;
                    break;
                }
                if (icc.getQuantidade() > 0) {
                    icList.remove(dell);
                }
            }
            if (!existe) {
                icList.add(ic);
            }

//            for (int i = 0; i < icList.size(); i++) {
//                ItemCompra icc = icList.get(i);
//                if (icc.getQuantidade() > 0) {
//                    icc.setQuantidade(icc.getQuantidade() - 1);
//                    icList.remove(dell);
//                }
//
//            }
        } catch (ProdutoNaoEncontradoException | SQLException ex) {
            Logger.getLogger(AdcionarCarrinho.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.getRequestDispatcher("/produtos/catalogo.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
