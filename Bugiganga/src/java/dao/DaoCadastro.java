package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.clienteBean;

/**
 *
 * @author Joelma
 *
 */
public class DaoCadastro {

    public Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5433/bugiganga", "postgres", "123456");
        } catch (SQLException | ClassNotFoundException e) {
        }
        return connection;
    }

    public clienteBean save(int codigo, String nome, String cidade, String endereco, String telefone, String email, String cpf, String login, String senha) {

        /*
         * Isso é uma sql comum, os ? são os parâmetros que nós vamos adicionar
         * na base de dados
         */
        Connection c = this.getConnection();
        PreparedStatement ps = null;

        try {
            ps = c.prepareStatement("INSERT INTO cliente (codigo, nome, cidade, endereco, telefone, email, cpf, login, senha)" + " VALUES(?,?,?,?,?,?,?,?,?)");

            //Adiciona o valor do parâmetro da sql
            ps.setInt(1, codigo);
            ps.setString(2, nome);
            ps.setString(3, cidade);
            ps.setString(4, endereco);
            ps.setString(5, telefone);
            ps.setString(6, email);
            ps.setString(7, cpf);
            ps.setString(8, login);
            ps.setString(9, senha);

            int r = ps.executeUpdate();

            if (r == 1) {
                clienteBean user = new clienteBean();
                user.setCodigo(codigo);
                user.setNome(nome);
                user.setCidade(cidade);
                user.setEndereco(endereco);
                user.setTelefone(telefone);
                user.setEmail(email);
                user.setCpf(cpf);
                user.setLogin(login);
                user.setSenha(senha);
                ps.close();
                c.close();
                return user;
            }

        } catch (SQLException e) {
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {;
                }
                ps = null;
            }
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {;
                }
                c = null;
            }
        }
        return null;
    }
}
