package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.clienteBean;

/**
 *
 * @author Joelma
 *
 */
public class DaoUsuario {

    public Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5433/bugiganga", "postgres", "123456");
        } catch (SQLException | ClassNotFoundException e) {
        }
        return connection;
    }

    @SuppressWarnings("empty-statement")
    public clienteBean getUsuario(String login, String senha) {
        System.out.println(login);
        System.out.println(senha);
        Connection c = this.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = c.prepareStatement("select codigo, nome, cidade, endereco, telefone, email, cpf from cliente where login = ? and senha = ?");
            ps.setString(1, login);
            ps.setString(2, senha);

            rs = ps.executeQuery();

            if (rs.next()) {
                clienteBean user = new clienteBean();
                user.setCodigo(rs.getInt("codigo"));
                user.setNome(rs.getString("nome"));
                user.setCidade(rs.getString("cidade"));
                user.setEndereco(rs.getString("endereco"));
                user.setTelefone(rs.getString("telefone"));
                user.setEmail(rs.getString("email"));
                user.setCpf(rs.getString("cpf"));
                user.setLogin(login);
                user.setSenha(senha);

                return user;
            }
        } catch (SQLException e) {
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {;
                }
                rs = null;
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {;
                }
                ps = null;
            }
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {;
                }
                c = null;
            }
        }
        return null;
    }

}
