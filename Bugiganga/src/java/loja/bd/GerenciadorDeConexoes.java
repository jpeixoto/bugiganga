package loja.bd;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Joelma
 */
public class GerenciadorDeConexoes {

    public static java.sql.Connection getConexao() {

        Connection connection = null;// atributo do tipo Connection 
        String driverName = "com.mysql.jdbc.Driver";
        String serverName = "localhost";// caminho do servidor do BD 
        String mydatabase = "bugiganga";// nome do seu banco de dados 
        String url = "jdbc:mysql://" + serverName + "/" + mydatabase;
        String username = "root";// nome de um usuário de seu BD       
        String password = "123456";// sua senha de acesso 

        try {
            Class.forName(driverName);
            connection = (Connection) DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException | SQLException e) {
        }
        return connection;

    }

    public static void main(String[] args) {

        java.sql.Connection conexao = getConexao();
        System.out.println("Conexão feita!");

    }

}
