/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loja.bd;

/**
 *
 * @author Joelma
 */
import bugiganga.negocio.Produto;
import bugiganga.negocio.excecoes.ProdutoNaoEncontradoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RepositorioProdutosJDBC {

    private final Connection conexaoBD;

    public RepositorioProdutosJDBC() {
        this.conexaoBD = GerenciadorDeConexoes.getConexao();
    }

    public Produto getProduto(String idProduto) throws ProdutoNaoEncontradoException, SQLException {
        Produto produto = null;

        try {
            PreparedStatement comandoSQL;
            comandoSQL = conexaoBD.prepareStatement("select * from produto where idProduto=?");
            comandoSQL.setString(1, idProduto);
            ResultSet rs = comandoSQL.executeQuery();
            if (rs.next()) {
                produto = new Produto();
                produto.setIdProduto(rs.getString("idProduto"));
                produto.setNome(rs.getString("nome"));
                produto.setPreco(rs.getDouble("preco"));
                produto.setQuantidade(rs.getInt("quantidade"));
                produto.setDescricao(rs.getString("descricao"));

                return produto;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Produto> getProdutos() throws Exception {

        List<Produto> produtos = new ArrayList<Produto>();
        try {
            PreparedStatement p = conexaoBD.prepareStatement("select * from produto");
            ResultSet rs = p.executeQuery();
            while (rs.next()) {

                String id = rs.getString("idProduto");
                String name = rs.getString("nome");
                double valor = rs.getDouble("preco");
                int qnt = rs.getInt("quantidade");
                String dec = rs.getString("descricao");

                System.out.println(name);

                Produto produto = new Produto(id, name, valor, qnt, dec);
                produtos.add(produto);
            }
            rs.close();
            p.close();
        } catch (SQLException e) {

        }
        return produtos;
    }

//  public List<Produto> buscarProdutos(String nome){
//      
//      
//      return null;
//}
//  public void diminuirQuantidade(String idProduto, int quantidade) throws ProdutoNaoEncontradoException { 
////Executa um comando SQL de modo que a quantidade de produtos em estoque 
////seja diminuida da quantidade passada como parâmetro do método
//}
}
