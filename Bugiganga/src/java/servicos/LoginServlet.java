/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servicos;

import bugiganga.negocio.Bugiganga;
import bugiganga.negocio.ItemCompra;
import bugiganga.negocio.Produto;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.clienteBean;
import dao.DaoUsuario;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 *
 *
 * Servlet que implementa o login do usuário e a criação da sessão
 */
public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 7633293501883840556L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(); //obtem a sessao do usuario, caso exista

        clienteBean user = null;
        String login_form = request.getParameter("login"); // Pega o Login vindo do formulario
        String senha_form = request.getParameter("password"); //Pega a senha vinda do formulario

        try {
            DaoUsuario dao = new DaoUsuario(); //cria uma instancia do DAO usuario
            user = dao.getUsuario(login_form, senha_form);
        } catch (Exception e) {

        }

        //se nao encontrou usuario no banco, redireciona para a pagina de erro!
        if (user == null) {
            session.invalidate();

            // encaminhar para a página de login exibindo uma mensagem de login ou senha inválida
            request.setAttribute("mensagem", "Login ou senha inválida");
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else {
            Bugiganga loja = new Bugiganga();
            try {
                List<Produto> p = loja.getProdutos();
                request.setAttribute("produtoLista", p);
            } catch (Exception ex) {
                Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            //se o dao retornar um usuario, coloca o mesmo na sessao
            List<ItemCompra> carrinhoList = new ArrayList<ItemCompra>();
            session.setAttribute("carrinhoList", carrinhoList);

            session.setAttribute("user", user);
            request.getRequestDispatcher("produtos/catalogo.jsp").forward(request, response);

        }

    }

}
