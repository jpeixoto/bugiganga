/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicos;

import dao.DaoCadastro;
import javax.servlet.http.HttpServlet;
import model.clienteBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Joelma
 */
public class CadastroServlet extends HttpServlet {

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // busca o writer
        PrintWriter out = response.getWriter();
//        HttpSession session = request.getSession(); //obtem a sessao do usuario, caso exista

        Integer code_form = Integer.parseInt(request.getParameter("codigo"));
        String nome_form = request.getParameter("nome");
        String cidade_form = request.getParameter("cidade"); // Pega o Login vindo do formulario
        String endereco_form = request.getParameter("endereco");
        String telefone_form = request.getParameter("telefone"); // Pega o Login vindo do formulario
        String email_form = request.getParameter("email");
        String cpf_form = request.getParameter("cpf");
        String login_form = request.getParameter("login"); // Pega o Login vindo do formulario
        String senha_form = request.getParameter("password"); //Pega a senha vinda do formulario

        clienteBean user = new clienteBean();

        user.setCodigo(code_form);
        user.setNome(nome_form);
        user.setCidade(cidade_form);
        user.setEndereco(endereco_form);
        user.setTelefone(telefone_form);
        user.setEmail(email_form);
        user.setCpf(cpf_form);
        user.setLogin(login_form);
        user.setSenha(senha_form);

        DaoCadastro dao = new DaoCadastro(); //cria uma instancia do DAO usuario
        dao.save(code_form, nome_form, cidade_form, endereco_form, telefone_form, email_form, cpf_form, login_form, senha_form);


        request.setAttribute("mensagem", "<div style='color: blue'>Cadastro realizado com sucesso</div>");
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

}
