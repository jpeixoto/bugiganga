<%-- 
    Document   : catalogo
    Created on : 05/12/2013, 19:06:31
    Author     : Joelma
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="../jsp/cabecalho.jsp" %>


<jsp:useBean id="bugigangaBean" class="bugiganga.negocio.BugigangaBean" scope="page" >
    <jsp:setProperty name="bugigangaBean" property="sistema" value="${sistemaBugiganga}" />
</jsp:useBean>

<% if (session.getAttribute("carrinhoList") != null) {%>
<p><font color="orange" size="+1">Seu carrinho contém <%= ((java.util.List<bugiganga.negocio.ItemCompra>) session.getAttribute("carrinhoList")).size()%> produtos</font></p>
    <% java.util.List<bugiganga.negocio.ItemCompra> sc = (java.util.List<bugiganga.negocio.ItemCompra>) session.getAttribute("carrinhoList"); %>  
<div id="conteudo"></div>
<div id="carrinho">
    <table>
        <tr><td><font color="palevioletred">Produto</font></td><td><font color="palevioletred">Quantidade</font></td><td><font color="palevioletred">Preço</font></td><td><font color="palevioletred">Preço</font></td></tr>
                <% for (int i = 0; i < sc.size(); i++) {%> 
        <tr>
            <td bgcolor="#ffffaa">&nbsp;<%= sc.get(i).getItem().getNome()%>&nbsp;</td>
            <td bgcolor="#ffffaa">&nbsp;<%= sc.get(i).getQuantidade()%>&nbsp;</td>
            <td bgcolor="#ffffaa">&nbsp;<%= sc.get(i).getItem().getPreco()%>&nbsp;</td>
            <td bgcolor="#ffffaa"><c:url var="url" value="/AdcionarCarrinho" >
                    <c:param name="remover" value="<%= sc.get(i).getItem().getIdProduto()%>" />
                </c:url>
                <strong>&nbsp;<a href="${url}">Remover</a>&nbsp;</strong> 
            </td>
        </tr>
        <%}%>
    </table>
    <p><strong>
            <c:url var="url" value="/produtos/comprar" />
            <a href="${url}">Finalizar compras</a></strong></p>

    <button id="fechar">Fechar Carrinho</button>
</div>
<% } %>

<button id="ver">Visualizar Carrinho</button>

<br/>
<center>
    <h3>Produtos disponíveis para compra:</h3>
    <table summary="layout">
        <% java.util.List<bugiganga.negocio.Produto> pl = (java.util.List<bugiganga.negocio.Produto>) request.getAttribute("produtoLista"); %>
        <% for (int i = 0; pl != null && i < pl.size(); i++) {%>
        <tr>
            <td>
                <c:set var="idProduto" value="<%= pl.get(i).getIdProduto()%>" />
            </td>
            <td bgcolor="#ffffaa">
                <c:url var="url" value="/produtos/detalhesProduto.jsp" >
                    <c:param name="idProduto" value="<%= pl.get(i).getIdProduto()%>" />
                </c:url>
                <h2><strong><%= pl.get(i).getNome()%>&nbsp;</strong></h2>
            </td>
            <td bgcolor="#ffffaa">&nbsp;
                <fmt:formatNumber value="<%= pl.get(i).getPreco()%>" type="currency"/>
                &nbsp;&nbsp;
            </td>
            <td bgcolor="#ffffaa">
                <c:url var="url" value="/produtos/detalhesProduto.jsp" >
                    <c:param name="idProduto" value="<%= pl.get(i).getIdProduto()%>" />
                </c:url>&nbsp;
                <h4>&nbsp;<%= pl.get(i).getDescricao()%>&nbsp</h4>
            </td>
            <td bgcolor="#ffffaa">
                <c:url var="url" value="/AdcionarCarrinho" >&nbsp;
                    <c:param name="Add" value="<%= pl.get(i).getIdProduto()%>" />
                </c:url>
                <p><strong><a href="${url}">&nbsp;Adicionar produto ao carrinho&nbsp;</a></strong></p>
            </td>
        </tr>
        <%}%>
    </table>
</center>


<%@ include file="../jsp/rodape.jsp" %>
