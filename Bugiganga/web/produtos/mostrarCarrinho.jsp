<%-- 
    Document   : mostrarCarrinho
    Created on : 05/12/2013, 21:38:35
    Author     : Joelma
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="../jsp/cabecalho.jsp" %>

<jsp:useBean id="bugigangaBean" class="bugiganga.negocio.BugigangaBean" scope="page" >
    <jsp:setProperty name="bugigangaBean" property="sistema" value="${sistemaBugiganga}" />
</jsp:useBean>


<c:if test="${param.limpar == 'limpar'}">
    <font color="red" size="+2"><strong> 
        O carrinho de compras foi esvaziado!
    </strong><br>&nbsp;<br></font>
</c:if>

<c:if test="${param.remover != '0'}">
    <c:set var="id" value="${param.remover}"/>
    <jsp:setProperty name="BugigangaBean" property="idProduto" value="${id}" />
    <c:set var="produtoRemovido" value="${bugigangaBean.produto}" />
    <font color="red" size="+2">O seguinte livro foi removido do carrinho: 
    <em>${produtoRemovido.nome}</em>.
    <br>&nbsp;<br> 
    </font>
</c:if>


<c:if test="${sessionScope.cart.numeroItens > 0}"> 
    <font size="+2">Quantidade de itens do carrinho: ${sessionScope.cart.numeroItens}
    <c:if test="${sessionScope.cart.numeroItens == 1}"> 
        produto.
    </c:if>
    <c:if test="${sessionScope.cart.numeroItens > 1}"> 
        produtos.
    </c:if>

    </font><br>&nbsp;
    <table summary="layout"> 
        <tr> 
            <th align=left>Quantidade</th> 
            <th align=left>Nome</th> 
            <th align=left>Preço</th> 
        </tr>

        <c:forEach var="itemCompra" items="${sessionScope.cart.itens}">
            <c:set var="produto" value="${itemCompra.item}" />
            <tr> 
                <td align="right" bgcolor="#ffffff"> 
                    ${itemCompra.quantidade}
                </td> 
                <td bgcolor="#ffffaa"> 
                    <c:url var="url" value="/produtos/detalhesProduto" >
                        <c:param name="idProduto" value="${produto.idProduto}" />
                        <c:param name="Clear" value="0" />
                    </c:url>
                    <strong><a href="${url}">${produto.nome}</a></strong> 
                </td> 
                <td bgcolor="#ffffaa" align="right"> 
                    <fmt:formatNumber value="${produto.preco}" type="currency"/>&nbsp;</td>  


                <td bgcolor="#ffffaa"> 
                    <c:url var="url" value="/produtos/mostrarCarrinho" >
                        <c:param name="remover" value="${produto.idProduto}" />
                    </c:url>
                    <strong><a href="${url}">Remover</a></strong> 
                </td>

            </tr>
        </c:forEach>

        <tr><td colspan="5" bgcolor="#ffffff"> 
                <br></td></tr> 
        <div>        <tr> 
                <td colspan="2" align="right" bgcolor="#ffffff"> 
                    Subtotal</td> 
                <td bgcolor="#ffffaa" align="right"> 
                    <fmt:formatNumber value="${sessionScope.cart.total}" type="currency"/>   
                </td>
                <td><br></td>
            </tr></div></table> 
    <p>&nbsp;<p>


        <c:url var="url" value="/produtos/catalogo" >
            <c:param name="Add" value="" />
        </c:url>
        <strong><a href="${url}">Continuar comprando</a>&nbsp;&nbsp;&nbsp; 

            <c:url var="url" value="/produtos/comprar" />
            <a href="${url}">Finalizar compra</a>&nbsp;&nbsp;&nbsp;

            <c:url var="url" value="/produtos/mostrarCarrinho" >
                <c:param name="limpar" value="limpar" />
                <c:param name="remover" value="0" />
            </c:url>
            <a href="${url}">Esvaziar carrinho</a></strong>

    </c:if>


    <%@ include file="../jsp/rodape.jsp" %>