<%-- 
    Document   : detalhesProduto
    Created on : 05/12/2013, 20:50:24
    Author     : Joelma
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="../jsp/cabecalho.jsp" %>


<jsp:useBean id="bugigangaBean" class="bugiganga.negocio.BugigangaBean" scope="page" >
    <jsp:setProperty name="bugigangaBean" property="sistema" value="${sistemaBugiganga}" />
</jsp:useBean>


<c:if test="${!empty param.idProduto}">
    <c:set var="id" value="${param.idProduto}"/>
    <jsp:setProperty name="bugigangaBean" property="idProduto" value="${id}" />
    <c:set var="produto" value="${bugugangaBean.produto}" />

    <h2>${produto.nome}</h2>

    <h4>Descrição</h4>

    <blockquote>${produto.descricao}</blockquote>

    <h4>Preço: <fmt:formatNumber value="${produto.preco}" type="currency"/></h4>


    <c:url var="url" value="/produtos/catalogo" >
        <c:param name="Add" value="${id}" />
    </c:url>

    <p><strong><a href="${url}">Adicionar ao carrinho</a>&nbsp; &nbsp; &nbsp;

        </c:if>


        <c:url var="url" value="/produtos/catalogo" >
            <c:param name="Add" value="" />
        </c:url>

        <a href="${url}">Continuar comprando</a></strong></p>

<%@ include file="../jsp/rodape.jsp" %>
