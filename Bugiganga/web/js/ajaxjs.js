/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {

//código da página de visualizar carrinho
$("#ver").click(function() {
    $("#conteudo").load("titulo.html");
    $("#carrinho, #conteudo").slideDown(1000);
    $("#ver").hide(1000);

});

$("#fechar").click(function() {
    $("#conteudo, #carrinho").slideUp(1000);
    $("#ver").show(1000);
});



//campo de "código" obrigatório
//aceita somente números
$(".numero").bind("keyup blur focus", function(e) {
    e.preventDefault();
    var expre = /[^\d]/g;
    $(this).val($(this).val().replace(expre, ''));
});

//campos obrigatórios
$("#Cadastrar").click(function() {
    var valor = document.getElementById("codigo").value;
    if ((valor == null) || (valor == "")) {
        $("#codigo_preenchido").show();
        return false;
    }
    return true;
});



$("#Cadastrar").click(function() {
var valor = document.getElementById("telefone").value;
if ((valor == null) || (valor == "")) {
    $("#telefone_preenchido").show();
    return false;
}
return true;
});



$("#Cadastrar").click(function() {
var valor = document.getElementById("email").value;
if ((valor == null) || (valor == "")) {
    $("#email_preenchido").show();
    return false;
}
return true;
});



$("#Cadastrar").click(function() {
var valor = document.getElementById("email").value;
if ((valor == null) || (valor == "")) {
    $("#email_preenchido").show();
    return false;
}
return true;
});



$("#Cadastrar").click(function() {
var valor = document.getElementById("cpf").value;
if ((valor == null) || (valor == "")) {
    $("#cpf_preenchido").show();
    return false;
}
return true;
});


$("#Cadastrar").click(function() {
var valor = document.getElementById("login").value;
if ((valor == null) || (valor == "")) {
    $("#login_preenchido").show();
    return false;
}
return true;
});


$("#Cadastrar").click(function() {
var valor = document.getElementById("password").value;
if ((valor == null) || (valor == "")) {
    $("#senha_preenchida").show();
    return false;
}
return true;
});


//código do formulário de cadastro de usuário
//campo "nome" obrigatório
$("#Cadastrar").click(function() {
    var valor = document.getElementById("nome").value;
    if ((valor == null) || (valor == "")) {
        $("#nome_preenchido").show();
        $("#nome_tamanho").hide();
        return false;
    } else if ((valor.length < 3)) {
        $("#nome_tamanho").show();
        $("#nome_preenchido").hide();
        return false;
    }
    return true;
});

//marca com borda vermelha os campos obrigatórios
$(document).ready(function() {
    $("#codigo, #nome, #telefone, #email, #cpf, #login, #password").blur(function() {
        if ($(this).val() == "")
        {
            $(this).css({"border-color": "#F00", "padding": "2px"});
        }
    });
});


//verifica cpf
$('#cpf').validacpf();


//validador de email
$('#Cadastrar').click(function() {
    // captura email
    var email = $("#email").val();
    // expressão regular
    var emailValido = /^.+@.+\..{2,}$/;

    if (!emailValido.test(email))
    {
        alert('Email inválido!');
    }
});


//verificação da força da senha
$('#password').pstrength();

});


//adiciona mascara ao telefone
function MascaraTelefone(telefone) {
    if (mascaraInteiro(telefone) == false) {
        event.returnValue = false;
    }
    return formataCampo(telefone, '(00) 0000-0000', event);
}


//valida telefone
function ValidaTelefone(telefone) {
    exp = /\(\d{2}\)\ \d{4}\-\d{4}/
    if (!exp.test(telefone.value))
        alert('Numero de Telefone Invalido!');
}


//valida numero inteiro com mascara
function mascaraInteiro() {
    if (event.keyCode < 48 || event.keyCode > 57) {
        event.returnValue = false;
        return false;
    }
    return true;
}


//formata de forma generica os campos
function formataCampo(campo, Mascara, evento) {
    var boleanoMascara;

    var Digitato = evento.keyCode;
    exp = /\-|\.|\/|\(|\)| /g
    campoSoNumeros = campo.value.toString().replace(exp, "");

    var posicaoCampo = 0;
    var NovoValorCampo = "";
    var TamanhoMascara = campoSoNumeros.length;
    ;

    if (Digitato != 8) { // backspace 
        for (i = 0; i <= TamanhoMascara; i++) {
            boleanoMascara = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
                    || (Mascara.charAt(i) == "/"))
            boleanoMascara = boleanoMascara || ((Mascara.charAt(i) == "(")
                    || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " "))
            if (boleanoMascara) {
                NovoValorCampo += Mascara.charAt(i);
                TamanhoMascara++;
            } else {
                NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
                posicaoCampo++;
            }
        }
        campo.value = NovoValorCampo;
        return true;
    } else {
        return true;
    }
}

//adiciona mascara ao CPF
function MascaraCPF(cpf) {
    if (mascaraInteiro(cpf) == false) {
        event.returnValue = false;
    }
    return formataCampo(cpf, '000.000.000-00', event);
}


//valida cpf
jQuery.fn.validacpf = function() {
    this.change(function() {
        cpf = $(this).val();
        if (!cpf) {
            return false;
        }
        erro = new String;
        cpfv = cpf;
        if (cpfv.length == 14 || cpfv.length == 11) {
            cpfv = cpfv.replace('.', '');
            cpfv = cpfv.replace('.', '');
            cpfv = cpfv.replace('-', '');

            var nonNumbers = /\D/;

            if (nonNumbers.test(cpfv)) {
                erro = "A verificacao de CPF suporta apenas números!";
            } else {
                if (cpfv == "00000000000" ||
                        cpfv == "11111111111" ||
                        cpfv == "22222222222" ||
                        cpfv == "33333333333" ||
                        cpfv == "44444444444" ||
                        cpfv == "55555555555" ||
                        cpfv == "66666666666" ||
                        cpfv == "77777777777" ||
                        cpfv == "88888888888" ||
                        cpfv == "99999999999") {

                    erro = "Número de CPF inválido!"
                }
                var a = [];
                var b = new Number;
                var c = 11;

                for (i = 0; i < 11; i++) {
                    a[i] = cpfv.charAt(i);
                    if (i < 9)
                        b += (a[i] * --c);
                }
                if ((x = b % 11) < 2) {
                    a[9] = 0
                } else {
                    a[9] = 11 - x
                }
                b = 0;
                c = 11;
                for (y = 0; y < 10; y++)
                    b += (a[y] * c--);

                if ((x = b % 11) < 2) {
                    a[10] = 0;
                } else {
                    a[10] = 11 - x;
                }
                if ((cpfv.charAt(9) != a[9]) || (cpfv.charAt(10) != a[10])) {
                    erro = "Número de CPF inválido.";
                }
            }
        } else {
            if (cpfv.length == 0) {
                return false;
            } else {
                erro = "Número de CPF inválido.";
            }
        }
        if (erro.length > 0) {
            $(this).val('');
            alert(erro);
            setTimeout(function() {
                $(this).focus();
            }, 100);
            return false;
        }
        return $(this);
    });
}